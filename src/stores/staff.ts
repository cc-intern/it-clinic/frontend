import { ref } from 'vue'
import { defineStore } from 'pinia'
import Staff from '@/types/staff'
import staffService from '@/service/staff'
import { useMessageStore } from './message'
export const useStaffStore = defineStore('staff', () => {
  const messageStore = useMessageStore()
  const staffs = ref<Staff[]>([])
  const addStaffDialog = ref(false)
  const newStaffRole = ref<string>()
  const newStaffUsername = ref<string>()
  async function getStaffs() {
    try {
      const res = await staffService.getStaffs()
      staffs.value = res.data
      console.log(staffs.value)
    } catch (error) {
      console.log(error)
      messageStore.showError('ไม่สามารถดึงข้อมูล เจ้าหน้าที่ ได้')
    }
  }

  async function delStaff(staff: Staff) {
    try {
      await staffService.delStaff(staff.staff_id)
      await getStaffs()
    } catch (error) {
      console.log(error)
      messageStore.showError('ไม่สามารถลบข้อมูล เจ้าหน้าที่ ได้')
    }
  }

  async function addStaff() {
    try {
      await staffService.addStaff(newStaffRole.value!, newStaffUsername.value!)
      newStaffRole.value = undefined
      newStaffUsername.value = undefined
      await getStaffs()
      addStaffDialog.value = false
    } catch (error) {
      console.log(error)
      messageStore.showError('ไม่สามารถเพิ่มข้อมูล เจ้าหน้าที่ ได้')
    }
  }

  return { staffs, getStaffs, delStaff, addStaff, addStaffDialog, newStaffRole, newStaffUsername }
})
