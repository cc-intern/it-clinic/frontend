import { defineStore } from 'pinia'
import authService from '@/service/auth'
import { useMessageStore } from './message'
import { ref } from 'vue'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()

  const loginDialog = ref(false)
  const getUser = () => {
    const userString = localStorage.getItem('user')
    console.log(userString)
    if (!userString) return null
    const user = JSON.parse(userString ?? '')
    return user
  }

  const isLogin = () => {
    const user = localStorage.getItem('user')
    if (user) {
      return true
    }
    return false
  }
  const login = async (username: string, password: string) => {
    try {
      const res = await authService.login(username, password)
      localStorage.setItem('access_token', res.data.access_token)
      localStorage.setItem('user', JSON.stringify(res.data.user))
      console.log('Success')
      loginDialog.value = false
      window.location.reload()
    } catch (error) {
      messageStore.showError('ไม่สามารถ login ได้')
    }
  }

  const logout = () => {
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    window.location.reload()
  }
  return { login, logout, getUser, isLogin, loginDialog }
})
