import { ref } from 'vue'
import { defineStore } from 'pinia'
import formService from '@/service/form'
import { useMessageStore } from './message'
import Form from '@/types/form'
import Question from '@/types/question'
import { useLoadingStore } from './loading'
export const useFormStore = defineStore('form', () => {
  const messageStore = useMessageStore()
  const loadingStore = useLoadingStore()
  const forms = ref<Form[]>([])
  const ques_content = ref<string>()
  const selectedForm = ref<number>()
  const newQuestion = ref<string>()
  const addFormDialog = ref(false)
  const addFormType = ref<string>()
  const addFormName = ref<string>()
  async function addForm() {
    try {
      addFormDialog.value = false
      loadingStore.isLoading = true
      await formService.addForms(addFormType.value!, addFormName.value!)
      addFormType.value = ''
      addFormName.value = ''
      await getForms()
      addFormDialog.value = false
      loadingStore.isLoading = false
    } catch (error) {
      messageStore.showError('ไม่สามารถเพิ่มแบบสอบถามได้')
    }
  }
  async function getForms() {
    try {
      const res = await formService.getForms()
      forms.value = res.data
      console.log(forms.value)
    } catch (error) {
      console.log(error)
      messageStore.showError('ไม่สามารถดึงข้อมูล ฟอร์ม ได้')
    }
  }

  async function addQuestion() {
    console.log(selectedForm.value)
    await formService.addQuestion(selectedForm.value!, newQuestion.value!)
    newQuestion.value = ''
    await getForms()
  }

  async function delQuestion(question: Question) {
    await formService.delQuestion(question.ques_id)
    await getForms()
  }

  async function delForm(form: Form) {
    try {
      loadingStore.isLoading = true
      await formService.delForm(form.form_id)
      await getForms()
      loadingStore.isLoading = false
    } catch (err) {
      messageStore.showError('ไม่สามารถลบแบบประเมินได้')
    }
  }
  return {
    getForms,
    delQuestion,
    forms,
    ques_content,
    selectedForm,
    addQuestion,
    newQuestion,
    addFormDialog,
    addFormType,
    addFormName,
    addForm,
    delForm
  }
})
