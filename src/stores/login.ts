import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoginStore = defineStore('login', () => {
  const loginDialog = ref(false)

  return { loginDialog }
})
