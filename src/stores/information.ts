import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import Information from '@/types/information'
import informationService from '@/service/information'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
export const useInformationStore = defineStore('information', () => {
  const messageStore = useMessageStore()
  const informations = ref<Information[]>([])
  const loadingStore = useLoadingStore()
  const newInfo = ref<string>()
  async function getInformations() {
    try {
      const res = await informationService.getInformations()
      informations.value = res.data
      console.log(informations.value)
    } catch (error) {
      console.log(error)
      messageStore.showError('ไม่สามารถดึงข้อมูล Information ได้')
    }
  }

  async function delInformation(info: Information) {
    try {
      loadingStore.isLoading = true
      await informationService.delInformation(info.info_id)
      await getInformations()
      loadingStore.isLoading = false
    } catch (error) {
      loadingStore.isLoading = false
      messageStore.showError('ไม่สามารถลบข้อมูลได้')
    }
  }

  async function addInfo() {
    try {
      loadingStore.isLoading = true
      await informationService.addInformation(newInfo.value!)
      newInfo.value = undefined
      await getInformations()
      loadingStore.isLoading = false
    } catch (error) {
      loadingStore.isLoading = false
      messageStore.showError('ไม่สามารถเพิ่มข้อมูลได้')
    }
  }
  return { informations, getInformations, delInformation, newInfo, addInfo }
})
