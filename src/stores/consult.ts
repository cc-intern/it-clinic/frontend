import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import consultService from '@/service/consult'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'
import { useAuthStore } from './auth'
import Consult from '@/types/consult'
import consult from '@/service/consult'

export const useConsultStore = defineStore('consult', () => {
  const messageStore = useMessageStore()
  const loadingStore = useLoadingStore()
  const consult_type = ref<string>()
  const consult_question = ref<string>()
  const authStore = useAuthStore()
  const trackingConsult = ref<Consult[]>([])
  async function openConsult() {
    try {
      loadingStore.isLoading = true
      const user = await authStore.getUser()
      await consultService.addConsult(user, consult_type.value!, consult_question.value!)
      consult_type.value = undefined
      consult_question.value = undefined
      loadingStore.isLoading = false
    } catch (error) {
      loadingStore.isLoading = false
      messageStore.showError('ไม่สามารถบันทึกการขอคำปรึกษาได้')
    }
  }

  async function getTrackingConsult() {
    try {
      const user = await authStore.getUser()
      const res = await consultService.findTrackingConsult(user)
      trackingConsult.value = res.data
      console.log(trackingConsult.value)
    } catch (error) {
      loadingStore.isLoading = false
      messageStore.showError('ไม่สามารถโหลดข้อมูลการขอคำปรึกษาได้')
    }
  }
  return { consult_type, consult_question, openConsult, trackingConsult, getTrackingConsult }
})
