import OrderInfo from './order_info'

export default interface Order {
  order_id: number

  order_bookingDate: Date

  order_username: string

  order_createdDate: Date

  order_updatedDate: Date

  order_deletedDate: Date

  // order_ratings: OrderRating[];

  // order_process: OrderProcess[];

  // order_forms: OrderForm[];

  order_info: OrderInfo[]
}
