import Information from './information'
import Order from './order'

export default interface OrderInfo {
  order_info_id: number

  order_info_answer: string

  infoId: number

  orderId: number

  infomation: Information

  order: Order
}
