import OrderInfo from './order_info'

export default interface Information {
  info_id: number

  info_content: string

  order_info: OrderInfo[]
}
