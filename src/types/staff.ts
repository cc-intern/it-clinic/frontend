import Consult from './consult'

export default interface Staff {
  staff_id: number

  staff_username: string

  staff_role: string

  // orders: Order[];

  consults: Consult[]

  // orders_process: OrderProcess[]
}
