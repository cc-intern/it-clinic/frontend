export default interface User {
  username: string

  fullname: string

  email: string

  role: string
}
