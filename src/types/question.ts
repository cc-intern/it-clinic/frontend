import Form from './form'

export default interface Question {
  ques_id: number

  ques_question: string

  ques_avgRating: number

  formId: number

  form: Form

  // order_ratings: OrderRating[];

  // consult_ratings: ConsultRating[];
}
