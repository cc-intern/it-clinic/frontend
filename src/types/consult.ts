export default interface Consult {
  consult_id: number

  consult_username: string

  consult_fullname: string

  consult_email: string

  consult_type: string

  consult_question: string

  consult_answer: string

  consult_status: string

  consult_createdDate: Date

  consult_updatedDate: Date

  consult_deletedDate: Date
}
