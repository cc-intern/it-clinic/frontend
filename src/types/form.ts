import Question from './question'

export default interface Form {
  form_id: number

  form_type: string

  form_name: string

  form_avgRating: number

  questions: Question[]

  // order_forms: OrderForm[]

  // consult_forms: ConsultForm[]
}
