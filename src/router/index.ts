import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'main',
      components: {
        default: () => import('../views/MainView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    },
    {
      path: '/form',
      name: 'form',
      components: {
        default: () => import('@/views/form/FormView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    },
    {
      path: '/reserve',
      name: 'reserve',
      components: {
        default: () => import('@/views/order/ReserveView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    },
    {
      path: '/consult',
      name: 'consult',
      components: {
        default: () => import('@/views/consult/ConsultView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    },
    {
      path: '/info',
      name: 'info',
      components: {
        default: () => import('@/views/information/InformationView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    },
    {
      path: '/tracking',
      name: 'tracking',
      components: {
        default: () => import('@/views/tracking/TrackingView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    },
    {
      path: '/staff',
      name: 'staff',
      components: {
        default: () => import('@/views/staff/StaffView.vue'),
        menu: () => import('@/components/Menu/MainMenu.vue'),
        header: () => import('@/components/Header/MainHeader.vue')
      }
    }
  ]
})

export default router
