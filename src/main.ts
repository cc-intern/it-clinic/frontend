import { createApp } from 'vue'
import App from './App.vue'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { createPinia } from 'pinia'
import router from './router'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'

// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap/dist/js/bootstrap.js'
// import './assets/bs_node_modules/jquery/jquery-3.2.1.min.js'
// import './assets/bs_node_modules/bootstrap/dist_/js/bootstrap.min.js'
// // import './assets/bs_node_modules/sticky-kit-master/dist/sticky-kit.min.js'
// import './assets/bs_node_modules/sparkline/jquery.sparkline.min.js'
// import './scripts/js/perfect-scrollbar.jquery.min.js'
// import './scripts/js/custom.min.js'
// import './scripts/js/sidebarmenu.js'
// import './scripts/js/pages/mask.init.js'
// // import './assets/bs_node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js'
// // import './assets/bs_node_modules/popper/popper.min.js'
// import './scripts/css/style.css'
// import './scripts/css/style.min.css'

const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi
    }
  }
})

const app = createApp(App)
app.use(vuetify)
app.use(createPinia())
app.use(router)
app.mount('#app')
