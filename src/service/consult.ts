import User from '@/types/user'
import http from './axios'
function addConsult(user: User, consult_type: string, consult_question: string) {
  return http.post('/consults', {
    consult_username: user.username,
    consult_fullname: user.fullname,
    consult_email: user.email,
    consult_type: consult_type,
    consult_question: consult_question
  })
}

function findTrackingConsult(user: User) {
  return http.get(`/consults/tracking/${user.username}`)
}
export default { addConsult, findTrackingConsult }
