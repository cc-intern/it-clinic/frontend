import http from './axios'
function getForms() {
  return http.get('/forms')
}

function addForms(addFormType: string, addFormName: string) {
  return http.post('/forms', { form_type: addFormType, form_name: addFormName })
}

function addQuestion(form_id: number, ques_question: string) {
  return http.patch(`/forms/addQuestion/${form_id}`, { ques_question: ques_question })
}

function delQuestion(ques_id: number) {
  return http.delete(`/forms/delQuestion/${ques_id}`)
}

function delForm(form_id: number) {
  return http.delete(`/forms/${form_id}`)
}
export default { getForms, addQuestion, addForms, delQuestion, delForm }
