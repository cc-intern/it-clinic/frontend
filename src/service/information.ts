import http from './axios'
function getInformations() {
  return http.get('/informations')
}
function addInformation(info_content: string) {
  return http.post('/informations', { info_content: info_content })
}

function delInformation(info_id: number) {
  return http.delete(`/informations/${info_id}`)
}
export default { getInformations, addInformation, delInformation }
