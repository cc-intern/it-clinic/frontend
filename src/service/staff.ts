import http from './axios'

function getStaffs() {
  return http.get('/staffs')
}

function addStaff(staff_username: string, staff_role: string) {
  return http.post('/staffs', { staff_username: staff_username, staff_role: staff_role })
}

function delStaff(staff_id: number) {
  return http.delete(`/staffs/${staff_id}`)
}
export default { getStaffs, addStaff, delStaff }
